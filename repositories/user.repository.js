const fs = require('fs');

const saveData = (data) => {
    // код по сохранению данных в БД
    const jsonContent = JSON.stringify(data);
    fs.writeFile(__dirname + "/../db/userlist.json", jsonContent, 'utf8', function (err) {
        if (err) {
            console.log("Can't save file");
            return false;
        }
    });
};

const readData = (cb) => {
    fs.readFile(__dirname + "/../db/userlist.json", "utf8", function (err, data) {
        if (err) {
            console.log("Can't read file");
            cb(err, null);
        }
        cb(null,data);
    });
};

module.exports = {
    saveData,
    readData
};