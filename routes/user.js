const express = require('express');
const router = express.Router();

const {
    getUsersFromDb,
    getUserByIdFromDb,
    saveUserToDb,
    changeUserInDb,
    deleteUserInDb
} = require("../services/user.service");

const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/:id', function(req, res, next) {
  const { id } = req.params;
  const callback = (err, user) => {
      if (err) {
          res.status(400).send(`Can't find user with id=${id}`);
      }
        res.send(user);
    };
    getUserByIdFromDb(id, callback);
});

router.get('/', function(req, res, next) {
  const callback = (err, data) => {
    if (err) {
        res.status(400).send(`Can't get any users ;)`);
    }
      res.send(data);
  };
  getUsersFromDb(callback);
});

router.post('/', isAuthorized, function(req, res, next) {
    const callback = (err, data) => {
        if (err) {
            res.status(400).send(err.message ? err.message : 'Some error');
        }
        res.send(data);
    };
    saveUserToDb(req.body, callback);
});

router.put('/', function(req, res, next) {
    const callback = (err, data) => {
        if (err) {
            res.status(400).send(err.message ? err.message : 'Some error');
        }
        res.send(data);
    };
    changeUserInDb(req.body, callback);
});

router.delete('/:id', function(req, res, next) {
    const { id } = req.params;
    const callback = (err, data) => {
        if (err) {
            res.status(400).send(err.message ? err.message : 'Some error');
        }
        res.send(data);
    };
    deleteUserInDb(id, callback);
});

module.exports = router;
