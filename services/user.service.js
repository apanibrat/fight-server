const { saveData, readData } = require("../repositories/user.repository");
const usersDb = require("../db/userlist.json");

const getName = (user) => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const getUsers = () => {
  if (usersDb) {
    return usersDb
  }
  return null;
};

const getUsersFromDb = (cb) => {
  const callback = (err, data) => {
    if (err) {
      return cb(err, null);
    }
    const usersArr = JSON.parse(data);
    return cb(null, usersArr);
  };
  readData(callback);
};

const getUserByIdFromDb = (id, cb) => {
  const callback = (err, data) => {
    if (err) {
      return cb(err, null);
    }
    const usersArr = JSON.parse(data);
    const index = usersArr.findIndex(item => item._id === id);
   
    if (index === -1) {
      return cb(true, null);
    }
    const user = usersArr[index];
    return cb(null, user);
  };
  readData(callback);
};

const getUserById = (id) => {
  const index = usersDb.findIndex(item => item._id === id);
  if (index === -1) {
    return null;
  }
  return usersDb[index];
};

const changeUser = (user) => {
  const index = usersDb.findIndex(item => item._id === user._id);
  if (index === -1) {
    return null;
  }
    usersDb[index] = {...usersDb[index], ...user };
    saveData(usersDb);
  return usersDb[index];
};

const deleteUserById = (id) => {
  const index = usersDb.findIndex(item => item._id === id);
  if (index === -1) {
    return null;
  }
    usersDb.splice(index, 1);
    saveData(usersDb);
  return usersDb;
};

const saveName = (user) => {
  if (user) {
    return saveData(user.name);
  } else {
    return null;
  }
};

const saveUser = (user) => {
  if (user && user._id) {
    const index = usersDb.findIndex(item => item._id === user._id);
    if (index === -1) {
      usersDb.push(user);
        saveData(usersDb);
      return user;
    }
  }
    return null;
};

const saveUserToDb = (user, cb) => {
  if (!user || !user._id) {
    return cb(new Error('User data not valid'));
  }
  const callback = (err, data) => {
    if (err) {
      return cb(err);
    }
    const usersArr = JSON.parse(data);
    const index = usersArr.findIndex(item => item._id === user._id);
    
    if (index === -1) {
      usersArr.push(user);
      saveData(usersArr);
      return cb(null, usersArr);
    }
    return cb(new Error('User already exist'));
  };
  readData(callback);
};

const changeUserInDb = (user, cb) => {
  if (!user || !user._id) {
    return cb(new Error('User data not valid'));
  }
  const callback = (err, data) => {
    if (err) {
      return cb(err);
    }
    const usersArr = JSON.parse(data);
    const index = usersArr.findIndex(item => item._id === user._id);
    
    if (index !== -1) {
      usersArr[index] = {...usersArr[index], ...user };
      saveData(usersArr);
      return cb(null, usersArr);
    }
    return cb(new Error(`Can't find user with id=${user._id}`));
  };
  readData(callback);
};

const deleteUserInDb = (id, cb) => {
  const callback = (err, data) => {
    if (err) {
      return cb(err);
    }
    const usersArr = JSON.parse(data);
    const index = usersArr.findIndex(item => item._id === id);
    
    if (index !== -1) {
      usersArr.splice(index, 1);
      saveData(usersArr);
      return cb(null, usersArr);
    }
    return cb(new Error(`Can't find user with id=${id}`));
  };
  readData(callback);
};

module.exports = {
  getName,
  saveName,
  getUsers,
  getUserById,
  saveUser,
  changeUser,
  deleteUserById,
  getUsersFromDb,
  getUserByIdFromDb,
  saveUserToDb,
  changeUserInDb,
  deleteUserInDb,
};